package ru.usenkovsv.hackatons.bcscd2021.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.utility.DockerImageName;

import java.util.Map;

public class KafkaContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private final KafkaContainer container = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:7.0.0"));

    @Override
    public void initialize(@NotNull ConfigurableApplicationContext applicationContext) {
        container.start();

        Map<String, String> kafkaProperties = Map.of(
                "my-app.kafka.bootstrap-address", container.getBootstrapServers(),
                "my-app.kafka.consumer-group-id", "test-group-id");
        TestPropertyValues
                .of(kafkaProperties)
                .applyTo(applicationContext.getEnvironment());
    }
}
