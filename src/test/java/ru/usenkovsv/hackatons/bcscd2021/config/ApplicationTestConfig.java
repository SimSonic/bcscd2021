package ru.usenkovsv.hackatons.bcscd2021.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mockito.Answers;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import ru.usenkovsv.hackatons.bcscd2021.server.MyService;

@TestConfiguration
@RequiredArgsConstructor
@Slf4j
public class ApplicationTestConfig {

    @SpyBean
    public MyService myService;

    /*
    @Primary
    @Bean
    public MyService myServiceSpy(MyService myService) {
        // На случай, если сервис упакован в тонны прокси (sleuth, свои аспекты, т.п.)
        return Mockito.mock(MyService.class, AdditionalAnswers.delegatesTo(myService));
    }
    */
}
