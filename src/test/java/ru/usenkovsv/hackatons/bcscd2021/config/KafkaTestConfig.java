package ru.usenkovsv.hackatons.bcscd2021.config;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.stereotype.Component;
import ru.usenkovsv.hackatons.bcscd2021.config.setup.MyApplicationProperties;
import ru.usenkovsv.hackatons.bcscd2021.server.kafka.MyTopics;
import ru.usenkovsv.hackatons.bcscd2021.server.kafka.dto.KafkaInDto;
import ru.usenkovsv.hackatons.bcscd2021.server.kafka.dto.KafkaOutDto;

import java.util.HashMap;
import java.util.Map;

@TestConfiguration
@RequiredArgsConstructor
public class KafkaTestConfig {

    @Bean
    public ProducerFactory<String, KafkaInDto> testProducerFactory(MyApplicationProperties applicationProperties) {
        var kafkaProperties = applicationProperties.kafka();

        Map<String, Object> properties = new HashMap<>();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.bootstrapAddress());
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        return new DefaultKafkaProducerFactory<>(properties);
    }

    @Bean
    public KafkaTemplate<String, KafkaInDto> testKafkaTemplate(
            ProducerFactory<String, KafkaInDto> testProducerFactory
    ) {
        return new KafkaTemplate<>(testProducerFactory);
    }

    @Bean
    public ConsumerFactory<String, KafkaOutDto> testConsumerFactory(MyApplicationProperties applicationProperties) {
        var kafkaProperties = applicationProperties.kafka();

        Map<String, Object> properties = new HashMap<>();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.bootstrapAddress());
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, kafkaProperties.consumerGroupId());
        return new DefaultKafkaConsumerFactory<>(properties, new StringDeserializer(), new JsonDeserializer<>(KafkaOutDto.class));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, KafkaOutDto> testListenerContainerFactory(
            ConsumerFactory<String, KafkaOutDto> testConsumerFactory
    ) {
        var factory = new ConcurrentKafkaListenerContainerFactory<String, KafkaOutDto>();
        factory.setConsumerFactory(testConsumerFactory);
        return factory;
    }

    @Component
    @RequiredArgsConstructor
    public static class TestKafka {

        private final KafkaTemplate<String, KafkaInDto> testKafkaTemplate;

        @KafkaListener(topics = MyTopics.OUT_TOPIC)
        public void resend() {
            KafkaInDto response = KafkaInDto.builder()
                    .responseValue("RESPONSE")
                    .build();
            testKafkaTemplate.send(MyTopics.IN_TOPIC, response);
        }
    }
}
