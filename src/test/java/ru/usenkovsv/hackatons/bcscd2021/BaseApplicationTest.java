package ru.usenkovsv.hackatons.bcscd2021;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.usenkovsv.hackatons.bcscd2021.config.ApplicationTestConfig;
import ru.usenkovsv.hackatons.bcscd2021.config.FailFastContextInitializer;
import ru.usenkovsv.hackatons.bcscd2021.config.KafkaContextInitializer;
import ru.usenkovsv.hackatons.bcscd2021.config.KafkaTestConfig;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = {
                ApplicationTestConfig.class,
                // DatabaseTestConfig.class,
                KafkaTestConfig.class
        })
@ContextConfiguration(initializers = {
        FailFastContextInitializer.class,
        KafkaContextInitializer.class
})
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class BaseApplicationTest {

    @Autowired
    private ObjectMapper objectMapper;

    @SneakyThrows
    protected String toJson(Object payload) {
        return objectMapper.writeValueAsString(payload);
    }
}
