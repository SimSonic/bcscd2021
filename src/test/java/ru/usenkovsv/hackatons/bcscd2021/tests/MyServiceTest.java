package ru.usenkovsv.hackatons.bcscd2021.tests;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.testcontainers.shaded.org.awaitility.Awaitility;
import ru.usenkovsv.hackatons.bcscd2021.BaseApplicationTest;
import ru.usenkovsv.hackatons.bcscd2021.server.MyService;

import java.time.Duration;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
public class MyServiceTest extends BaseApplicationTest {

    @Test
    void testMyServiceMethod(@Autowired MyService myService) {
        AtomicBoolean processed = new AtomicBoolean();
        Mockito
                .doAnswer(invocation -> {
                    processed.set(true);
                    return null;
                })
                .when(myService)
                .onKafkaResponse(ArgumentMatchers.any());

        myService.requestUpdate();

        Awaitility.await()
                .atMost(Duration.ofSeconds(5))
                .pollDelay(Duration.ofMillis(100))
                .until(processed::get);
    }
}
