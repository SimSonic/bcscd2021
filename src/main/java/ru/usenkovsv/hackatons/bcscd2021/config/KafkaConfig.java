package ru.usenkovsv.hackatons.bcscd2021.config;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;
import ru.usenkovsv.hackatons.bcscd2021.config.setup.MyApplicationProperties;
import ru.usenkovsv.hackatons.bcscd2021.server.kafka.MyTopics;
import ru.usenkovsv.hackatons.bcscd2021.server.kafka.dto.KafkaInDto;
import ru.usenkovsv.hackatons.bcscd2021.server.kafka.dto.KafkaOutDto;

import java.util.HashMap;
import java.util.Map;

@Configuration
@RequiredArgsConstructor
public class KafkaConfig {

    @Bean
    public KafkaAdmin kafkaAdmin(MyApplicationProperties applicationProperties) {
        var kafka = applicationProperties.kafka();

        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, kafka.bootstrapAddress());
        return new KafkaAdmin(configs);
    }

    @Bean
    public KafkaAdmin.NewTopics newTopics() {
        return new KafkaAdmin.NewTopics(
                TopicBuilder.name(MyTopics.OUT_TOPIC)
                        .partitions(1)
                        .replicas(1)
                        .build(),
                TopicBuilder.name(MyTopics.IN_TOPIC)
                        .partitions(1)
                        .replicas(1)
                        .build()
        );
    }

    @Bean
    public ProducerFactory<String, KafkaOutDto> kafkaProducerFactory(MyApplicationProperties applicationProperties) {
        var kafkaProperties = applicationProperties.kafka();

        Map<String, Object> properties = new HashMap<>();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.bootstrapAddress());
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        return new DefaultKafkaProducerFactory<>(properties);
    }

    @Bean
    public KafkaTemplate<String, KafkaOutDto> kafkaTemplate(
            ProducerFactory<String, KafkaOutDto> kafkaProducerFactory
    ) {
        return new KafkaTemplate<>(kafkaProducerFactory);
    }

    @Bean
    public ConsumerFactory<String, KafkaInDto> kafkaConsumerFactory(MyApplicationProperties applicationProperties) {
        var kafkaProperties = applicationProperties.kafka();

        Map<String, Object> properties = new HashMap<>();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.bootstrapAddress());
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, kafkaProperties.consumerGroupId());
        return new DefaultKafkaConsumerFactory<>(properties, new StringDeserializer(), new JsonDeserializer<>(KafkaInDto.class));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, KafkaInDto> kafkaListenerContainerFactory(
            ConsumerFactory<String, KafkaInDto> kafkaConsumerFactory
    ) {
        var factory = new ConcurrentKafkaListenerContainerFactory<String, KafkaInDto>();
        factory.setConsumerFactory(kafkaConsumerFactory);
        return factory;
    }
}
