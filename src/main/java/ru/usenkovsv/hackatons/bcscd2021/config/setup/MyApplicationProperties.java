package ru.usenkovsv.hackatons.bcscd2021.config.setup;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ConfigurationProperties("my-app")
@Validated
public record MyApplicationProperties(
        @NestedConfigurationProperty @NotNull MyKafkaProperties kafka
) {

    public static record MyKafkaProperties(
            @NotBlank String bootstrapAddress,
            @NotBlank String consumerGroupId
    ) {

    }
}
