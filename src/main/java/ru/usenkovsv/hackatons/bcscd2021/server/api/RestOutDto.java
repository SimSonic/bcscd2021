package ru.usenkovsv.hackatons.bcscd2021.server.api;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class RestOutDto {

    String responseValue;
}
