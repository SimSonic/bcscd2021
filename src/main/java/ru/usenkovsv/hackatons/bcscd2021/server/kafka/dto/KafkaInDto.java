package ru.usenkovsv.hackatons.bcscd2021.server.kafka.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class KafkaInDto {

    String responseValue;
}
