package ru.usenkovsv.hackatons.bcscd2021.server;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Profile("!test")
@Component
@RequiredArgsConstructor
@Slf4j
public class MyScheduler {

    private final MyService myService;

    @Scheduled(fixedDelay = 500L)
    void requestUpdate() {
        myService.requestUpdate();
    }
}
