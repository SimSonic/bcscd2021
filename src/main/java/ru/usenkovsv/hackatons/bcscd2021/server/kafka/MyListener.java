package ru.usenkovsv.hackatons.bcscd2021.server.kafka;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import ru.usenkovsv.hackatons.bcscd2021.server.MyService;
import ru.usenkovsv.hackatons.bcscd2021.server.kafka.dto.KafkaInDto;

@Component
@RequiredArgsConstructor
@Slf4j
public class MyListener {

    private final MyService myService;

    @KafkaListener(topics = MyTopics.IN_TOPIC)
    void listener(KafkaInDto payload) {
        log.debug("Received payload: {}", payload);
        myService.onKafkaResponse(payload);
    }
}
