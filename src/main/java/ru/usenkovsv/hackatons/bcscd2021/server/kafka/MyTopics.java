package ru.usenkovsv.hackatons.bcscd2021.server.kafka;

import lombok.experimental.UtilityClass;

@UtilityClass
public class MyTopics {

    public final String OUT_TOPIC = "request-topic";
    public final String IN_TOPIC = "response-topic";
}
