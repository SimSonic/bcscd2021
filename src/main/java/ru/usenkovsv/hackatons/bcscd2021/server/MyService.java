package ru.usenkovsv.hackatons.bcscd2021.server;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.usenkovsv.hackatons.bcscd2021.server.kafka.KafkaService;
import ru.usenkovsv.hackatons.bcscd2021.server.kafka.dto.KafkaInDto;

import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class MyService {

    private final KafkaService kafkaService;

    public void requestUpdate() {
        String requestId = UUID.randomUUID().toString();
        kafkaService.sendRequest(requestId);
    }

    public void onKafkaResponse(KafkaInDto payload) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }
}
