package ru.usenkovsv.hackatons.bcscd2021.server.kafka;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import ru.usenkovsv.hackatons.bcscd2021.server.kafka.dto.KafkaOutDto;

@Service
@RequiredArgsConstructor
@Slf4j
public class KafkaService {

    private final KafkaTemplate<String, KafkaOutDto> kafkaOutDtoKafkaTemplate;

    public void sendRequest(String requestId) {
        KafkaOutDto kafkaOutDto = KafkaOutDto.builder()
                .requestId(requestId)
                .build();
        kafkaOutDtoKafkaTemplate.send(MyTopics.OUT_TOPIC, kafkaOutDto);
    }
}
