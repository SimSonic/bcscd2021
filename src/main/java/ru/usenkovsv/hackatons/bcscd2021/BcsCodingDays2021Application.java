package ru.usenkovsv.hackatons.bcscd2021;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.metrics.buffering.BufferingApplicationStartup;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import ru.usenkovsv.hackatons.bcscd2021.config.setup.MyApplicationProperties;

@EnableAsync
@EnableCaching
@EnableScheduling
@EnableConfigurationProperties({
        MyApplicationProperties.class
})
@SpringBootApplication(
        scanBasePackageClasses = BcsCodingDays2021Application.class,
        proxyBeanMethods = false
)
public class BcsCodingDays2021Application {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplicationBuilder()
                .bannerMode(Banner.Mode.OFF)
                .web(WebApplicationType.SERVLET)
                .sources(BcsCodingDays2021Application.class)
                .build();

        //noinspection MagicNumber
        application.setApplicationStartup(new BufferingApplicationStartup(10240));

        //noinspection resource
        application.run(args);
    }
}
